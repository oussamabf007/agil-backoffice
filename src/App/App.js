import {
  createMuiTheme,
  CssBaseline,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core";
import Header from "../components/Header";
import SideMenu from "../components/SideMenu";
import "./App.css";
import Evenement from "../pages/Evenement/Evenement";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#08090C",
      light: "#292A2E",
    },
    secondary: {
      main: "#EED629",
      light: "#EEEC61",
    },
    background: {
      default: "#343a40",
    },
    shape: {
      borderRadius: "12px",
    },
    overrides: {
      MuiAppBar: {
        root: {
          transform: "translateZ(0)",
        },
      },
    },
    props: {
      MuiIconButton: {
        disableRipple: true,
      },
    },
  },
});

const useStyles = makeStyles({
  appMain: {
    paddingLeft: "320px",
    width: "100%",
  },
});

function App() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <SideMenu />
      <div className={classes.appMain}>
        <Header />
        <Evenement />
      </div>
      <CssBaseline />
    </ThemeProvider>
  );
}

export default App;
