import React from "react";
import CreateEvenement from "./CreateEvenement";
import PageHeader from "../../components/PageHeader";
import PeopleOutlineTwoToneIcon from "@material-ui/icons/PeopleOutlineTwoTone";

export default function Evenement() {
  return (
    <>
      <PageHeader
        title="new evenement"
        subtitle="description"
        icon={<PeopleOutlineTwoToneIcon fontSize="large" />}
      />
      <CreateEvenement />;
    </>
  );
}
